banque_de_mot = mots = ["fromage","alambique","casserole","programme","television","logiciel","avion","gourmandise"\
        ,"telechargement","illegalite","instrument","tondeuse","ordinateur","programmation","technologie",\
        "diffusion","estampage","navigation","hasardeux","fondations","artistique","utilisation","imbuvable",\
        "legume","innovation","constitution","iconique","evidence","invitation","cavite","lampadaire","limonade"\
                ,"bouteille","concours","culture","psychologie","cardiologue","pharmaceutique","laboratoire","scolaire"\
                ,"rasoir","medicament","perfusion","pansement","forage","aiguille","costume","danser","contemporain"\
                ,"mondialisation","environnement","ombrelle","vetement","sentiment","congelateur","spatule","chandelier"\
                ,"bateau","commandant","paquerette","coquelicot","robinetterie","armoiries","boutique","fantome","plaisanterie"\
                ,"ironique","electricite","ingenieur","infirmiere","informatique","biologie","citoyennete","chaussette","confiseries"\
                ,"glacier","bistrot","opticien","elegant","aquatique","piscine","romantique","antiquite","automobile","italienne"]


import random
from tkinter import *
from tkinter import font
import time





liste_Des_Lettres_A_Trouver = []
liste_Des_Lettres_Trouvées = []
liste_Des_Lettres_Proposées = []





#fonctions
def lettre_dans_mot(proposition, mot_a_trouver):

    """la fonction parcourt la liste des lettres à trouver et retient l'emplacement dans le mot de la lettre proposée si elle y est.""" 

    position = []

    position_actuelle = 0
    for lettre_mot in mot_a_trouver:
        if lettre_mot == proposition:
            position.append(position_actuelle)
        position_actuelle += 1
    return position



def choix_mot():

    """la fonction choix_mot prend un mot au hasard dans la banque de mots"""

    mot_choisi = random.choice(banque_de_mot)
    return mot_choisi



def remplace_lettres(lettre_dans_mot, proposition):

    """cette fonction remplace les * de la liste de lettres trouvées par les véritables lettres lorsque elles sont trouvées"""

    liste_lettres_trouvées.replace(lettre_dans_mot, proposition)
    return liste_lettres_trouvées



mot_Caché = choix_mot()   #on lie une variable a une fonction


def generer_mot(mot_Caché):

    """cette fonction parcourt le mot qui a été choisi au hasard et la transforme en liste de lettres"""

    for x in mot_Caché:  #on parcourt chaque lettre du mot caché
        liste_Des_Lettres_A_Trouver.append(x)   #et à chaque nouvelle lettre, on l'ajoute à la liste: liste_Des_Lettres_A_Trouver
        liste_Des_Lettres_Trouvées.append('*')  #et on met une * dans la liste: liste_Des_Lettres_Trouvées pour avoir le même nombre de * que de lettre à trouver

    return liste_Des_Lettres_A_Trouver and liste_Des_Lettres_Trouvées  


mot_Genere = generer_mot(mot_Caché)



#description de la fenêtre graphique
fenetre = Tk()
fenetre.title('pendu')
fenetre.config(bg='light yellow')

font =font.Font(family='Purisa', size=18, weight='bold')

concombre=Canvas(fenetre)
concombre.pack(expand=YES)


cannevas_l_orig = cannevas_l = 300
cannevas_h_orig = cannevas_h = 400
zoom = 1.5
cannevas_l *= zoom
cannevas_h *= zoom

concombre.configure(background= "light yellow")
concombre.configure(width=cannevas_l, height=cannevas_h)



#afficher un titre
case_titre = Frame(fenetre, bg='light yellow')
case_titre.pack(side=TOP)
titre = Label(case_titre, text='jeux du pendu', font=("courrier", 40), bg='light yellow', fg='black')
titre.pack()

#bouton quitter
boite_bouton = Frame(fenetre, bg='light yellow')
boite_bouton.pack(side=BOTTOM)

bouton_quitter= Button(boite_bouton, text='quitter',bg="light yellow", font=("courrier", 25), command=fenetre.destroy)
bouton_quitter.pack()



print("".join(liste_Des_Lettres_Trouvées))   #on affiche la liste des lettres à trouver tout en les liant entre elles en enlevant les "".

chance = 11   #on met en place un compteur

continuer = True
while continuer == True:
    while chance > 0:

        if liste_Des_Lettres_Trouvées  != liste_Des_Lettres_A_Trouver:   #on créer une seconde condition qui va permettre de savoir quand le joueur a gagné
            proposition =input("entrez une lettre: ")
            proposition.lower()   #la proposition est mise en minuscule pour ne pas qu'il y ait de mal entendu

    
            for elem in liste_Des_Lettres_Proposées:
                if proposition == elem:
                    print("tu as déja proposé cette lettre")

            liste_Des_Lettres_Proposées.append(proposition)

            position_lettre = lettre_dans_mot(proposition, mot_Caché)   #la variable contient la fonction lettre_dans_mot avec pour paramètre  la lettre que va entrer le joueur et le mot à trouver

            c = 0
            for i in position_lettre:   #on parcourt la liste des lettres. 
                c += 1   #à chaque lettre on rajoute 1 à la variable c
        
            if c ==0:   #si c est egale à 0 cela veut dire que la liste de lettre ne contient pas la lettre proposée
                chance -= 1  
                if chance ==10:
                    concombre.create_line(50, 470, 150, 470, fill='black', width=6)
                if chance == 9:
                    concombre.create_line(100, 470, 100, 50, fill='black', width=6)
                if chance == 8:
                    concombre.create_line(50, 50, 350, 50, fill='black', width=6)
                if chance == 7:
                    concombre.create_line(100, 100, 150, 50, fill='black', width=6)
                if chance == 6:
                    concombre.create_line(300, 50, 300, 100, fill='black', width=6)
                if chance == 5:
                    concombre.create_oval(275, 100, 325, 150, fill='black', width=6)
                if chance == 4:
                    concombre.create_line(300, 150, 300, 300, fill='black', width=6)
                if chance == 3:
                    concombre.create_line(250, 200, 350, 200, fill='black', width=6)
                if chance == 2:
                    concombre.create_line(300, 300, 250, 350, fill='black', width=6)
                if chance == 1:
                    concombre.create_line(300, 300, 350, 350, fill='black', width=6)
                print("".join(liste_Des_Lettres_Trouvées))   #on affiche la liste des lettres trouvées dans le terminal
                print("nombre de chance qu'il te reste:", chance)

            else:
            
                if c == 1:   #si c est égal à 1 cela veux dire que la lettre proposée est une fois dans la liste des lettres à trouver
                    liste_Des_Lettres_Trouvées[position_lettre[0]] = proposition   #on attribue donc la proposition dans la liste des lettres trouvées à l'indice de la seule valeur que contient la liste position_lettre
                    print("nombre de chance qu'il te reste:", chance)   #....et dans le terminal
                    print("".join(liste_Des_Lettres_Trouvées))

                if c == 2:   #si c est égal à 2 cela veux dire qu'il y a 2 fois la lettre proposée dans la liste des lettres à trouver
                    liste_Des_Lettres_Trouvées[position_lettre[0]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa première valeur
                    liste_Des_Lettres_Trouvées[position_lettre[1]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa deuxième valeur
                    print("nombre de chance qu'il te reste:", chance)   #le nombre de chance 
                    print("".join(liste_Des_Lettres_Trouvées))   # et la liste des lettres trouvées dans la terminal

                if c == 3:   #si c est égal à 3 cela veux dire qu'il y a 2 fois la lettre proposée dans la liste des lettres à trouver
                    liste_Des_Lettres_Trouvées[position_lettre[0]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa première valeur
                    liste_Des_Lettres_Trouvées[position_lettre[1]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa deuxieme valeur
                    liste_Des_Lettres_Trouvées[position_lettre[2]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa troisieme valeur
                    print("nombre de chance qu'il te reste:", chance)
                    print("".join(liste_Des_Lettres_Trouvées))

                if c == 4:
                    liste_Des_Lettres_Trouvées[position_lettre[0]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa première valeur
                    liste_Des_Lettres_Trouvées[position_lettre[1]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa deuxième valeur
                    liste_Des_Lettres_Trouvées[position_lettre[2]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa troisieme valeur
                    liste_Des_Lettres_Trouvées[position_lettre[3]] = proposition   #on ajoute donc la proposition à l'indice dont la position lettre contient sa quatrième valeur
                    print("nombre de chance qu'il te reste:", chance)
                    print("".join(liste_Des_Lettres_Trouvées))
            
        else:
            print("bravo tu as trouvé le mot!")
            break #on sort de la boucle while
                      
    else:   #si le joueur a utilisé toutes ses chances
        print("tu vien d'utiliser ton dernier essais, tu as perdu! Le mot à trouver était:", mot_Caché)
        time.sleep(10)
        break   #on sort de la bouble while
        
    

        
    fenetre.mainloop()
    
    quit()



        