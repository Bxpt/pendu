**Présentation du jeux**

Bonjour, voici mon projet : LE PENDU.
Avant toutes choses il est nécessaire d'avoir python. Ensuite il faut télécharger le document pendu_jeu.py.
Ensuite on n'a plus qu'a éxecuter le document pendu_jeu.py dans python3.

Les règles du jeu sont simples : vous avez 10 chances pour trouver un mot comportant entre 3 et 12 lettres.

lors de l'exécutuion du programme une fenêtre va s'ouvrir, c'est l'interface graphique.
Pour proposer une lettre, il faut passer par le terminal. Si la lettre proposée est bien dans le mot, vous ne perdez pas de point.
Par contre, si la lettre n'est pas dans le mot, vous perdez un point.
Le dessin du pendu va se dessiner dans l'interface graphique.
Il est possible de consulter le nombre de chances restantes dans le terminal.
Lorsque la partie est terminée, l'interface graphique se ferme et un message s'affiche dans le terminal pour savoir si vous avez gagné ou perdu.
Il est possible de fermer l'interface graphique avec le bouton quitter.

